<?php /* Template Name: Goals */
get_header(); if(have_posts()):while(have_posts()):the_post(); ?>

<section class="page__section">
  <div class="container-fluid xl">
    <div class="page__title">
      <h1><?= the_title(); ?></h1>

      <?php if(get_the_content()): ?>
        <div class="page__title-content">
          <?= the_content(); ?>
        </div>
      <?php endif; ?>
    </div>

    <?php if(have_rows('goals')): ?>
      <ul class="goal__list">
        <?php while(have_rows('goals')):the_row(); ?>
          <?php $icon = get_sub_field('icon'); ?>

          <li class="goal__list__item">
            <img src="<?= $icon['url']; ?>" alt="<?= $icon['alt']; ?>" />
            <h4><?= the_sub_field('title'); ?></h4>
          </li>
        <?php endwhile; ?>
      </ul>
    <?php endif; ?>
  </div>
</section>

<?php endwhile; endif; get_footer(); ?>
