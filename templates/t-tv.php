<?php /* Template Name: TV */
get_header(); if(have_posts()):while(have_posts()):the_post(); ?>

<section class="page__section">
  <div class="container-fluid xl">
    <div class="page__title">
      <h1><?= the_title(); ?></h1>

      <?php if(get_the_content()): ?>
        <div class="page__title-content">
          <?= the_content(); ?>
        </div>
      <?php endif; ?>
    </div>

    <?php
      $vids = new WP_Query([
        'post_type' => 'tv',
        'posts_per_page' => -1,
        'meta_key'  => 'date_field',
        'orderby'   => 'meta_value_num',
        'order'     => 'ASC',
      ]);

      if($vids->have_posts()):
        echo '<div class="card__grid grid-3">';
          while($vids->have_posts()):$vids->the_post();
            get_template_part('parts/card','tv');
          endwhile;
        echo '</div>';
        wp_reset_postdata();
      endif;
    ?>
  </div>
</section>

<?php endwhile; endif; get_footer(); ?>
