<?php /* Template Name: Contact */
get_header();if(have_posts()):while(have_posts()):the_post(); ?>

<section class="page__section">
  <div class="container-fluid lg">
    <div class="page__title">
      <h1><?= the_title(); ?></h1>

      <?php if(get_the_content()): ?>
        <div class="page__title-content">
          <?= the_content(); ?>
        </div>
      <?php endif; ?>
    </div>

    <div class="contact__wrapper">
      <div class="contact__form">
        <?php get_template_part('parts/form','contact'); ?>
      </div>
      <div class="contact__details">
        <strong><?= the_field('company_name','option'); ?></strong>
        <address><?= the_field('company_address','option'); ?></address>
        <a href="tel:<?= the_field('company_phone','option'); ?>"><?= the_field('company_phone','option'); ?></a>
      </div>
    </div>
  </div>
</section>

<?php endwhile; endif; get_footer(); ?>
