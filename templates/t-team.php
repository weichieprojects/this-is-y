<?php /* Template Name: Team */
get_header(); if(have_posts()):while(have_posts()):the_post(); ?>

<section class="page__section">
  <div class="container-fluid xl">
    <div class="page__title">
      <h1><?= the_title(); ?></h1>

      <?php if(get_the_content()): ?>
        <div class="page__title-content">
          <?= the_content(); ?>
        </div>
      <?php endif; ?>
    </div>

    <?php if(have_rows('members')): ?>
      <ul class="member__list">
        <?php
          while(have_rows('members')):the_row();
            get_template_part('parts/card','team');
          endwhile;
        ?>
      </ul>
    <?php endif; ?>
  </div>
</section>

<?php endwhile; endif; get_footer(); ?>
