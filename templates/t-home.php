<?php /* Template Name: Home */
get_header(); if(have_posts()):while(have_posts()):the_post(); ?>

<section class="home__section">
  <?php $cover = get_field('home_banner_cover'); ?>
  <div class="home__hero" style="background-image:url('<?= $cover['url']; ?>');">
    <div class="filter"></div>
    <div class="container-fluid lg">
      <h1><?= the_title(); ?></h1>

      <?php if(get_field('home_banner_subtitle')): ?>
        <h2><?= the_field('home_banner_subtitle'); ?></h2>
      <?php endif; ?>
    </div>
  </div>

  <div class="home__sections">
    <div class="home__intro container-fluid sm">
      <?= the_content(); ?>
    </div>

    <?php
      if( have_rows('home_blocks') ):
        $counter = 1;
        while ( have_rows('home_blocks') ) : the_row();
          get_template_part('parts/blocks/home', get_row_layout(), ['white' => $counter]);
          $counter++;
        endwhile;
      endif;
    ?>
  </div>
</section>

<?php endwhile; endif; get_footer(); ?>
