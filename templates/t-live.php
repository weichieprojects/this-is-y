<?php /* Template Name: Live */
get_header(); if(have_posts()):while(have_posts()):the_post(); ?>

<section class="page__section">
  <div class="container-fluid xl">
    <div class="page__title">
      <h1><?= the_title(); ?></h1>

      <?php if(get_the_content()): ?>
        <div class="page__title-content">
          <?= the_content(); ?>
        </div>
      <?php endif; ?>
    </div>
    <?php
      $past = $_GET['past'];
      $today = date('Ymd');
      $compare = $_GET['past'] == 'true' ? '<' : '>=';
        $live = new WP_Query([
        'post_type' => 'live',
        'posts_per_page' => -1,
        'meta_query' => array(
          array(
            'key'		=> 'date_field',
            'compare'	=> $compare,
            'value'		=> $today,
          )
        ),
        'orderby'   => 'meta_value_num',
        'order'     => 'ASC',
      ]);

      if($live->have_posts()):
        echo '<div class="card__grid grid-3">';
          while($live->have_posts()):$live->the_post();
            get_template_part('parts/card','live');
          endwhile;
        echo '</div>';
        wp_reset_postdata();
      endif; ?>
  </div>
</section>

<?php endwhile; endif; get_footer(); ?>
