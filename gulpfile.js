const { src, dest, watch, series, parallel } = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const babel = require('gulp-babel');

const files = { 
  scssPath: 'assets/scss/**/*.scss',
  jsPath: 'assets/js/**/*.js'
}

function scssTask(){    
  return src(files.scssPath)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss([ autoprefixer(), cssnano() ]))
    .pipe(sourcemaps.write('.'))
    .pipe(dest('.'));
}

function jsTask(){
  return src([
    files.jsPath
    //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
    ])
    .pipe(concat('weichie.min.js'))
    .pipe(babel({ presets: ['@babel/preset-env'] }))
    .pipe(uglify())
    .pipe(dest('dist'));
}

function watchTask(){
  watch([files.scssPath, files.jsPath],
    {interval: 1000, usePolling: true}, //Makes docker work
    series(
      parallel(scssTask, jsTask)
    )
  );
}

exports.default = series(
    parallel(scssTask, jsTask), 
    watchTask
);
