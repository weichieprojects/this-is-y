<?php
add_action('after_setup_theme', 'weichieprojects_setup');
function weichieprojects_setup() {
    load_theme_textdomain('weichieprojects', get_template_directory() . '/languages');
    add_theme_support('title-tag');
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    register_nav_menus(array(
        'main-menu-left'        => __('Main Menu Left', 'weichieprojects'),
        'main-menu-right'       => __('Main Menu Right', 'weichieprojects'),
        'footer-menu'           => __('Footer Menu', 'weichieprojects'),
        // 'lang-menu'           => __('Language Menu', 'weichieprojects'),
    ));
}

add_action('wp_enqueue_scripts', 'weichieprojects_load_scripts');
function weichieprojects_load_scripts() {
    wp_register_style('screen', get_stylesheet_directory_uri().'/style.css', '', array(), 'screen');
    wp_enqueue_style('screen');

    // wp_register_style('custom-css', get_stylesheet_directory_uri().'/assets/css/custom.css', '', array(), 'screen');
    // wp_enqueue_style('custom-css');

    wp_enqueue_script("jQuery", "https://code.jquery.com/jquery-3.5.1.min.js", array(), '3.5.1', true );
    wp_enqueue_script("barba", "https://cdn.jsdelivr.net/npm/@barba/core",[],'',true);
    wp_enqueue_script("gsap", "https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.0/gsap.min.js", array(), '3.6.0', true );
    wp_enqueue_script('main', get_stylesheet_directory_uri().'/dist/weichie.min.js', ['jQuery'], '1.0.1', true);
}

add_filter('the_title', 'weichieprojects_title');
function weichieprojects_title($title) {
    if ($title == '') {
        return '&rarr;';
    } else {
        return $title;
    }
}

function weichie_edit_wp_footer($content) {
    return "<span>Website by <a href='https://weichie.com' target='_blank'>Weichie</a>.</span>";
}
add_filter('admin_footer_text', 'weichie_edit_wp_footer', 11);


add_filter('wp_title', 'weichieprojects_filter_wp_title');
function weichieprojects_filter_wp_title($title) {
    return $title . esc_attr(get_bloginfo('name'));
}

add_action('widgets_init', 'weichieprojects_widgets_init');
function weichieprojects_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar Widget Area', 'weichieprojects'),
        'id' => 'primary-widget-area',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => "</li>",
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
}

function weichieprojects_custom_pings($comment) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
    <?php
}

//remove author link in comments
function weichieprojects_remove_comment_author_link( $author_link, $author ) {
  return $author;
}
add_filter( 'get_comment_author_link', 'weichieprojects_remove_comment_author_link', 10, 2 );

add_action( 'admin_init', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}

add_filter('get_comments_number', 'weichieprojects_comments_number');
function weichieprojects_comments_number($count) {
    if (!is_admin()) {
        global $id;
        $comments_by_type =& separate_comments(get_comments('status=approve&post_id=' . $id));
        return count($comments_by_type['comment']);
    } else {
        return $count;
    }
}

add_action('init', 'register_custom_posts_init');
function register_custom_posts_init() {
    register_post_type('tv', array(
        'labels'             => array(
            'name'               => 'tv',
            'singular_name'      => 'tv',
            'menu_name'          => 'Y TV'
        ),
        'public'             => false,
        'publicly_queryable' => false,
        'show_in_menu'       => true,
        'show_ui'            => true,
        'capability_type'    => 'page',
        'hierarchical'       => true,
        'has_archive'        => false,
        'show_in_rest'       => true,
        'rewrite'            => array("with_front" => false),
        'menu_icon'          => 'dashicons-welcome-view-site',
        'taxonomies'         => array('category', 'post_tag'),
        'supports'           => array('title', 'excerpt', 'thumbnail', 'page-attributes', 'editor')
    ));

    register_post_type('live', array(
      'labels'             => array(
          'name'               => 'live',
          'singular_name'      => 'live',
          'menu_name'          => 'Y LIVE'
      ),
      'public'             => false,
      'publicly_queryable' => false,
      'show_in_menu'       => true,
      'show_ui'            => true,
      'capability_type'    => 'page',
      'hierarchical'       => true,
      'has_archive'        => false,
      'show_in_rest'       => true,
      'rewrite'            => array("with_front" => false),
      'menu_icon'          => 'dashicons-video-alt',
      'taxonomies'         => array('category', 'post_tag'),
      'supports'           => array('title', 'excerpt', 'thumbnail', 'page-attributes', 'editor')
  ));
}

if(function_exists('acf_add_options_page')){
    acf_add_options_page();
}

show_admin_bar(false);

function remove_comments(){
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'remove_comments' );
