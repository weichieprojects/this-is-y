<?php
  $headshot = get_sub_field('headshot');
  $member_cat = get_sub_field('category');
  $cat_field = 'category_' . $member_cat->term_id;
  $cat_logo = get_field('category_logo', $cat_field);
?>

<li class="card__member">
  <div class="card__member__cover" style="background-image: url('<?= $headshot['url']; ?>');"></div>
  <div class="card__member__content">
    <?php if($cat_logo): ?>
      <img src="<?= $cat_logo['sizes']['medium']; ?>" alt="<?= $member_cat->name; ?>">
    <?php else: ?>
      <span class="logo"><?= $member_cat->name; ?></span>
    <?php endif; ?>
    <h3><?= the_sub_field('name'); ?></h3>
    <span class="title"><?= the_sub_field('function'); ?></span>
  </div>
</li>

