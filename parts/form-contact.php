<form action="<?= get_home_url(); ?>" id="contact-form" method="post">
  <div class="input__group half">
    <label class="form__label" for="name">Naam</label>
    <input class="form__input" type="text" id="name" name="name" placeholder="Naam" />
  </div>
  <div class="input__group half">
    <label class="form__label" for="firstname">Voornaam</label>
    <input class="form__input" type="text" id="firstname" name="firstname" placeholder="Voornaam" />
  </div>
  <div class="input__group half">
    <label class="form__label" for="email">E-mail</label>
    <input class="form__input" type="email" id="email" name="email" placeholder="E-mail" />
  </div>
  <div class="input__group half">
    <label class="form__label" for="phone">GSM</label>
    <input class="form__input" type="text" id="phone" name="phone" placeholder="Gsm" />
  </div>
  <div class="input__group full">
    <label class="form__label" for="message">Bericht</label>
    <textarea class="form__input" rows="3" id="message" name="message" placeholder="Bericht"></textarea>
  </div>
  <div class="input__group full">
    <button type="submit" class="link__cta">Verzenden</button>
  </div>
</form>
