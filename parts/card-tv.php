<?php
  $unixtimestamp = strtotime(get_field('date_field'));
  $category = get_the_category();
  $cat_field = 'category_' . $category[0]->term_id;
  $cat_logo = get_field('category_logo', $cat_field);
  $link = get_field('link_field') ? get_field('link_field') : '#!';
  $target = get_field('link_field') ? '_blank' : '';
  $cover = get_field('default_thumbnail', $cat_field);
  $cover_link = get_the_post_thumbnail_url() ? get_the_post_thumbnail_url() : $cover['url'];
?>

<a href="<?= $link; ?>" class="card__tv" target="<?= $target; ?>">
  <div class="card__tv__cover" style="background-image: url('<?= $cover_link; ?>');"></div>
  <div class="card__tv__content">
    <?php if($cat_logo): ?>
      <img src="<?= $cat_logo['sizes']['medium']; ?>" alt="<?= $category[0]->name; ?>">
    <?php else: ?>
      <span class="logo"><?= $category[0]->name; ?></span>
    <?php endif; ?>

    <h3><?= the_title(); ?></h3>

    <div class="card__tv-footer">
      <?php if(get_field('link_field')): ?>
        <span class="link__cta">Bekijk de video</span>
      <?php else: ?>
        <span class="footer__date">
          <?= date_i18n( "l d.m.Y", $unixtimestamp ); ?>
          <?php if(get_field('start_hour')): ?>
            - <?= the_field('start_hour'); ?>U
          <?php endif; ?>
        </span>
      <?php endif; ?>
    </div>
  </div>
</a>
