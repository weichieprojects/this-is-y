<?php
  $white = ($args['white'] % 2 === 0) ? ' bg__white' : '';
  $cta = get_sub_field('link');
?>

<div class="section__wrapper<?= $white; ?>">
  <div class="container-fluid xl">
    <div class="home__text">
      <?= the_sub_field('content'); ?>

      <?php if($cta): ?>
        <a href="<?= $cta['url']; ?>" class="link__cta" target="<?= $cta['target']; ?>">
          <?= $cta['title']; ?>
        </a>
      <?php endif; ?>
    </div>
  </div>
</div>
