<?php
  $video_group = get_sub_field('video_group');
  $content_group = get_sub_field('video_content');

  $white = ($args['white'] % 2 === 0) ? ' bg__white' : '';
  $reverse = $content_group['reverse'] ? ' reverse' : '';
?>

<div class="section__wrapper<?= $white; ?>">
  <div class="container-fluid xl">
    <div class="home__peak<?= $reverse; ?>">
      <div class="home__peak__content">
        <?= $content_group['content']; ?>

        <?php if($content_group['button']): ?>
          <a href="<?= $content_group['button']['url']; ?>" class="link__cta" target="<?= $content_group['button']['target']; ?>">
            <?= $content_group['button']['title']; ?>
          </a>
        <?php endif; ?>
      </div>
      <div class="home__peak__video" data-modal="<?= $args['white']; ?>">
        <img src="<?= $video_group['video_poster']['url']; ?>" alt="<?= $video_group['video_poster']['alt']; ?>" />
        <svg viewBox="0 0 122 122"><g transform="translate(-554 -1305)"><circle cx="61" cy="61" r="61" transform="translate(554 1305)" fill="#fff"/><g transform="translate(636 1349) rotate(90)" fill="#191919"><path d="M 33.1419677734375 29.5 L 0.8580323457717896 29.5 L 17 1.014174699783325 L 33.1419677734375 29.5 Z" stroke="none"/><path d="M 17 2.028339385986328 L 1.716060638427734 29 L 32.28393936157227 29 L 17 2.028339385986328 M 17 0 L 34 30 L 0 30 L 17 0 Z" stroke="none" fill="#707070"/></g></g></svg>
      </div>
    </div>
  </div>
</div>

<section class="overlay" id="modal-<?= $args['white']; ?>" data-video="video-<?= $args['white']; ?>">
  <div class="modal__video">
    <a href="#!" class="close-member"></a>
    <div class="modal-content">
      <video width="400" controls muted id="video-<?= $args['white']; ?>">
        <source src="<?= $video_group['video_file']['url']; ?>" type="video/mp4">
        Your browser does not support HTML video.
      </video>
    </div>
  </div>
</section>
