<?php
  $photo_group = get_sub_field('photo_group');
  $content_group = get_sub_field('photo_content');

  $white = ($args['white'] % 2 === 0) ? ' bg__white' : '';
  $reverse = $content_group['reverse'] ? ' reverse' : '';
?>

<div class="section__wrapper<?= $white; ?>">
  <div class="container-fluid xl">
    <div class="home__peak<?= $reverse; ?>">
      <div class="home__peak__content">
        <?= $content_group['content']; ?>

        <?php if($content_group['button']): ?>
          <a href="<?= $content_group['button']['url']; ?>" class="link__cta" target="<?= $content_group['button']['target']; ?>">
            <?= $content_group['button']['title']; ?>
          </a>
        <?php endif; ?>
      </div>
      <div class="home__peak__video">
        <img src="<?= $photo_group['cover']['url']; ?>" alt="<?= $photo_group['cover']['alt']; ?>" />
        <?php if($photo_group['photo_caption']): ?>
          <span class="figcaption"><?= $photo_group['photo_caption']; ?></span>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
