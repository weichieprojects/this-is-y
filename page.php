<?php get_header(); if(have_posts()):while(have_posts()):the_post(); ?>

<section class="page__section">
  <div class="container-fluid xl">
    <div class="page__title">
      <h1><?= the_title(); ?></h1>
      <?= the_content(); ?>
    </div>
  </div>
</section>

<?php endwhile; endif; get_footer(); ?>
