<?php get_header(); ?>
<section id="content" >
    <?php if(have_posts()): ?>
        <header class="header">
            <h1 class="entry-title"><?php printf(__('Search Results for: %s', 'weichieprojects'), get_search_query()); ?></h1>
        </header>
        <?php while(have_posts()) : the_post(); ?>
            <?php get_template_part('_components/entry'); ?>
        <?php endwhile; ?>
    <?php else: ?>
        <article class="no-results not-found">
            <header class="header">
                <h2 class="entry-title">Nothing Found</h2>
            </header>
            <section class="entry-content">
                <p>Sorry, nothing matched your search. Please try again.</p>
                <?php get_search_form(); ?>
            </section>
        </article>
    <?php endif ?>
</section>
<?php get_footer(); ?>
