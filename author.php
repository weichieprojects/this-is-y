<? get_header(); ?>
<section id="content" >
    <header class="header">
        <? the_post(); ?>
        <h1 class="entry-title author"><? _e( 'Author Archives', 'nightowl' ); ?>: <? the_author_link(); ?></h1>
        <? if ( '' !=g et_the_author_meta( 'user_description' ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . get_the_author_meta( 'user_description' ) . '</div>' ); ?>
        <? rewind_posts(); ?>
    </header>
    <? while ( have_posts() ) { the_post(); ?>
        <? get_template_part( 'entry' ); ?>
    <? } ?>
    <? get_template_part( 'nav', 'below' ); ?>
</section>
<? get_sidebar(); ?>
<? get_footer(); ?>
