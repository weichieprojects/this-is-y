<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#0a0a0a">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">

	<link rel="stylesheet" href="https://use.typekit.net/pmw4qda.css">
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Prompt:ital,wght@0,300;0,400;0,600;1,400&display=swap" rel="stylesheet">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-RZEWJVDWY3"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-RZEWJVDWY3');
	</script>

	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TV5ZPDR');</script>

	<?php wp_head(); ?>
</head>

<body data-barba="wrapper">
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TV5ZPDR" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

	<header>
		<div class="container-fluid xl">
			<?php wp_nav_menu(['theme_location'=> 'main-menu-left', 'container'=> false, 'menu_class'=> 'menu__main']); ?>

			<div class="header__branding">
				<?php $logo = get_field('site_logo', 'option'); ?>
				<?php if($logo): ?>
					<a href="<?= home_url(); ?>">
						<img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>">
					</a>
				<?php endif; ?>
			</div>

			<?php wp_nav_menu(['theme_location'=> 'main-menu-right', 'container'=> false, 'menu_class'=> 'menu__main']); ?>

			<div class="hamburger">
				<span></span>
				<span></span>
				<span></span>
			</div>
		</div>

		<div class="mobile__menu">
			<div class="mobiel__menu__inner">
				<div class="mobile__menu__wrapper">
					<?php wp_nav_menu(['theme_location'=> 'main-menu-left', 'container'=> false, 'menu_class'=> 'mobile__nav']); ?>
					<?php wp_nav_menu(['theme_location'=> 'main-menu-right', 'container'=> false, 'menu_class'=> 'mobile__nav']); ?>
				</div>
			</div>
		</div>
	</header>

	<div data-barba="container">
