/*!
 * Bob Weichler - WeichieProjects
 * https://weichie.com
 */

$(function(){
	console.log("%c.: Website by Weichie.com :.", "color: #33bddb;");
	init();

	let BaseAnimation = {opacity: 0, y: 30, duration: 0.3,}

	barba.init({
		transitions: [{
			leave: data =>  gsap.timeline().to(data.current.container, BaseAnimation),
			enter: data => {
				return gsap.timeline()
					.from(data.next.container, {
						...BaseAnimation,
						onComplere: _ => {
							data.current.container.style.display = 'none'
							window.scrollTo(0,0)
						}
					})
				},
			afterEnter: _ => {
				init()
			}
		}]
	});

	/** Responsive Menu */
	$('.hamburger, .mobile__nav a').on('click', function() {
		$('body').toggleClass('locked');
		$('.hamburger').toggleClass('active');
		$('.mobile__menu').fadeToggle();
	})
});

function init() {
	/** Scroll to section */
	$('.goto').on('click', function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top
		}, 700);
	});


	/** Modal */
	$('.home__peak__video').on('click', function() {
		$(`#modal-${$(this).data('modal')}`).fadeIn();
		const video = document.getElementById(`video-${$(this).data('modal')}`);
		video.play();
	});

	$('.overlay').on('click', function (e) {
		if (e.target !== this) return;
		const video = document.getElementById(`${$(this).data('video')}`);
		video.pause();
		$(this).fadeOut();
	});

	/** Contact Form */
	$('#contact-form').on('submit', function(e) {
		e.preventDefault();
		$('.input__group').removeClass('error');

		$.post("/wp-content/themes/weichieprojects/send_mail.php", $(this).serialize(), function (data) {
			if (data.success) {
				barba.go('/thank-you')
			} else {
				if (data.error == 1) {
					const errors = data.notvalid;
					errors.forEach((ele) => $(`#${ele}`).parent().addClass('error'));
				} else if (data.error == 2) {
					console.log("It looks like there went something wrong with the server. Please try again in a few minutes.");
				}
			}
		}, 'json');
	});
}

/*----------------------------------- */
/* SCROLL FUNCTIONS	                 */
/*----------------------------------- */
// $(document).on('scroll', () => {
// 	const viewportHeight = $(window).height() | 0;
// 	const fromTop = $(document).scrollTop() | 0;
// 	const fromBottom = fromTop + viewportHeight;
// });


/*----------------------------------- */
/* HELPER FUNCTIONS	                 */
/*----------------------------------- */
