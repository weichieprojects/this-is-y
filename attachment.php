<? get_header(); ?>
<? global $post; ?>
<section id="content" >
    <? if ( have_posts() ) {
         while ( have_posts() ) { the_post(); ?>
            <header class="header">
                <h1 class="entry-title"><? the_title(); ?> <span class="meta-sep">|</span> <a href="<? echo get_permalink( $post->post_parent ); ?>" title="<? printf( __( 'Return to %s', 'nightowl' ), esc_html( get_the_title( $post->post_parent ), 1 ) ); ?>" rev="attachment"><span class="meta-nav">&larr; </span><? echo get_the_title( $post->post_parent ); ?></a></h1>
                <? edit_post_link(); ?>
                <? get_template_part( 'entry', 'meta' ); ?>
            </header>
            <article id="post-<? the_ID(); ?>" <? post_class(); ?>>
                <header class="header">
                    <nav id="nav-above" class="navigation" role="navigation">
                        <div class="nav-previous">
                            <? previous_image_link( false, '&larr;' ); ?>
                        </div>
                        <div class="nav-next">
                            <? next_image_link( false, '&rarr;' ); ?>
                        </div>
                    </nav>
                </header>
                <section class="entry-content">
                    <div class="entry-attachment">
                        <? if ( wp_attachment_is_image( $post->ID ) ) { $att_image = wp_get_attachment_image_src( $post->ID, "large" ); ?>
                            <p class="attachment"><a href="<? echo wp_get_attachment_url( $post->ID ); ?>" title="<? the_title(); ?>" rel="attachment"><img src="<? echo $att_image[0]; ?>" width="<? echo $att_image[1]; ?>" height="<? echo $att_image[2]; ?>" class="attachment-medium" alt="<? $post->post_excerpt; ?>" /></a>
                            </p>
                        <? } else { ?>
                        <a href="<? echo wp_get_attachment_url( $post->ID ); ?>" title="<? echo esc_html( get_the_title( $post->ID ), 1 ); ?>" rel="attachment">
                            <? echo basename( $post->guid ); ?></a>
                        <? } ?>
                    </div>
                    <div class="entry-caption">
                        <? if ( !empty( $post->post_excerpt ) ) the_excerpt(); ?></div>
                    <? if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
                </section>
            </article>
            <? comments_template(); ?>
        <? } ?>
    <? } ?>
</section>
<? get_sidebar(); ?>
<? get_footer(); ?>
