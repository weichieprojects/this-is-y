<? get_header(); ?>
<div class="section pjax-animation loading up">
    <div class="container-fluid lg error-container">
        <h1>That's a 404!</h1>
        <p>
            Apologies, the page you are looking for does not exist.
        </p>

        <div class="projects-outro text-center">
            <a href="/" class="btn btn-ghost btn-lg">
                <span>Return to homepage</span>
            </a>
        </div><!-- ./projects-outro -->
    </div>
</div>
<? get_footer(); ?>
