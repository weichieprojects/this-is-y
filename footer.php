</div><!-- ./barba-container -->

<footer>
	<div class="container-fluid xl">
		<div class="footer__copy">
			&copy; <?= the_field('company_name','option'); ?>
		</div>

		<?php $logo = get_field('site_logo', 'option'); ?>
		<?php if($logo): ?>
			<div class="footer__logo">
				<a href="<?= home_url(); ?>">
					<img src="<?= $logo['url']; ?>" alt="<?= $logo['alt']; ?>">
				</a>
			</div>
		<?php endif; ?>

		<?php
			wp_nav_menu([
				'theme_location' => 'footer-menu',
				'container' => false,
				'menu_class'=> 'footer__menu'
			]);
		?>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
